Portfolio Website
<hr>

### By Jon Stump
<img align="center" src="https://avatars2.githubusercontent.com/u/59323850?s=460&u=372c7d529b7379408ae54491ab3449b6e2f4d94d&v=4">

## Technologies Used
* Angular
* TypeScript
* NVM
* Node
* neovim

## Scope
* About me
* Hobbies and Skills
* Job History
* Education history
* Contact info
* Photo of myself

## Description

This is my portfolio website that includes projects, work history, skills, and information about me.

## Examples of use
* For sending to potential employers
* Linking in blog posts on medium
* Including in your resume for portfolio examples

## Setup/installation Requirements

Install instructions are not avaiable at this time since this project is being overhauled from it's previous github page variation. I recently have gotten into Angular and decided to see how it would work out making my portfolio page in it. Likely overkill, but I've been enjoying working with the framework so far.

Instructions to come once hosted.

## Sources Used
* [Angular Docs](https://angular.io/docs)
* [Angular Up & Running: Learning Angular Step by Step](https://www.amazon.com/Angular-Up-Running-Learning-Step/dp/1491999837/ref=sr_1_17?dchild=1&keywords=angular&qid=1630777205&s=books&sr=1-17)
* [Understand With NgFor Loop In Angular](https://www.c-sharpcorner.com/article/understanding-with-ngfor-loop-in-angular/)
* [Using ngFor loop](https://mdbootstrap.com/education/angular/agenda-app-5-ngfor/)

## To Do List
- [ ] Add profile photo component
- [ ] Add contact component
- [ ] Add overall CSS styling
- [ ] Update component CSS for specific use cases
- [ ] Update jonstump.com to host page
- [ ] Move data from front end to a secure backend
- [ ] Host projects on project page

## Known Bugs
- [ ] Hamburger menu for navbar does not display nav options

## License
Legal is working on it.

## Contact Information
[jmstump@gmail.com](mailto:jmstump@gmail.com)
