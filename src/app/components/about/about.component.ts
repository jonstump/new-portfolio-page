import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

    about =
  'Hi! My name is Jon Stump and I reside in the Pacific Northwest. I recently finished my web and mobile development course at Epicodus where I learned things like JavaScript, RESTful routing and APIs, React, Ruby, and Rails. Lately I have been working with Angular and Svelte on a couple projects. I also really nerd out on BASH scripting and systems. /I travel a lot and have been to multiple counties, my last one was Vietnam. I also read a lot and do my best to read across as many different topics as possible. I read around 30 books a year.';

    hobbies = ["Reading", "Video Games", "Gundam", "Dungeons & Dragons", "Tabletop Roleplaying Games"];

  constructor() { }

  ngOnInit(): void {
  }

}
