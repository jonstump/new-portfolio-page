import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projects: any[];

  constructor() {
  this.projects = [
    {
      tools: ["Angular","TypeScript","NVM","neovim","Docker"],
      title: "Herring & Robinson Book Binder Website",
      hosted: "http:website-to-go.here",
      description: "A website for Herring and Robinson Book Binders created in Angular to allow for customers to submit digital forms and track their orders. Currently does not have a backend, but some work has been done on a docker container for a more consistent development environment and ease of future development",
    },
    {
      tools: ["Ruby","Rails","SCSS","Faker","Rspec","Postgres"],
      title: "Gundam Model Collections",
      hosted: "https://cryptic-basin-12416.herokuapp.com/gunplas",
      description: "A website built during an Epicodus Team Week during the Ruby on Rails track. I worked on this with David Couch and Kerry Lang. The website is a demo of a site that would allow people to track what Gundam Model Kits, otherwise known as Gunpla, and review them withing that community.",
    }
  ];

  }


  ngOnInit(): void {
  }

}
