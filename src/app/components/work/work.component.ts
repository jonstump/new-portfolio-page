import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit {

  constructor() {
    this.works = [
       {
      company_name: "Hubb",
      title: "Software Engineer Intern",
      dates: "June 2021 - July 2021",
      location: "Vancouver WA",
      responsibilities: "Learning Angular and working with p5.js"},
      {
      company_name: "Oregon Department of Transportation",
      title: "Performance Analyst",
      dates: "July 2018 - Aug 2019",
      location: "Salem OR",
      responsibilities: "Working across business units to help improve analysis, KPIs, and processes."},
      {
      company_name: "Udacity",
      title: "Program Manager and Data Analyst",
      dates: "Feb 2016 - Jan 2018",
      location: "Sunnyvale CA",
      responsibilities: "Did Program and Data stuffs"},
      {
      company_name: "Electronic Arts",
      title: "Development Manager",
      dates: "Mar 2014 - Dec 2015",
      location: "Redwood City CA",
      responsibilities: "Worked with phones a lot"},
      {
      company_name: "zynga",
      title: "Business Analyst, Customer Support Data Analyst, Customer Support Representative",
      dates: "May 2011 - Jan 2014",
      location: "San Francisco CA",
      responsibilities: "Worked with customers"}
    ]
  }
  works: any[]

  ngOnInit(): void {
  }

}
